from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

import os

def enable_download_headless(browser,download_dir):
    browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    params = {'cmd':'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
    browser.execute("send_command", params)

if __name__ == '__main__':
    options = Options()
    options.add_argument('--no-sandbox')
    options.add_argument("--disable-notifications")
    options.add_argument('--verbose')
    options.add_experimental_option("prefs", {
        "download.default_directory": '/app',
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing_for_trusted_sources_enabled": False,
        "safebrowsing.enabled": False
    })
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-software-rasterizer')
    options.add_argument('--headless')
   # driver_path = f'{os.getcwd()}\\webdriver\\chromedriver.exe'
    driver = webdriver.Chrome(chrome_options=options)
    enable_download_headless(driver, '/app')

#inicializamos el navegador:

driver.get('https://www.ine.es/')

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/section[3]/div/ul/li[3]/div/a/p[1]/img')))\
    .click()

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/div[2]/div[2]/ul/li[1]/span')))\
    .click()

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/div[2]/div[2]/ul/li[1]/div/ul/li/table/tbody/tr[1]/th/a')))\
    .click()

WebDriverWait(driver,5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/section[2]/div/div/div[1]/ul/li/ul/li[1]/a')))\
    .click()

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/div[4]/div[2]/ol/li[1]/ol/li[1]/a[3]')))\
    .click()

WebDriverWait(driver, 5)\
    .until(ec.element_to_be_clickable((By.XPATH,
                                       '/html/body/div[1]/main/form/ul/li[1]/ul/li[1]/div/fieldset/select')))

seleccion = driver.find_element_by_xpath('/html/body/div[1]/main/form/ul/li[1]/ul/li[1]/div/fieldset/select')
seleccion = seleccion.text

with open('infoselect.txt', 'w', encoding='UTF-8') as inf:
    inf.write(seleccion)

driver.close()
